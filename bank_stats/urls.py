from django.conf.urls import patterns, include, url

from django.contrib import admin
from bank import views



admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.index, name='index'),
    url(r'^login/$', views.user_login, name='login'),
    url(r'^register/$', views.user_register, name='register'),
    url(r'^profile/$', views.show_profile, name='profile'),
    url(r'^logout/$',views.user_logout, name='logout'),
)
