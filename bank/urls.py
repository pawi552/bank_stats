# -*- coding: utf-8 -*-
__author__ = 'bartek'
__date__ = '8/15/14'
__project__ = 'bank_stats'

from django.conf.urls import patterns, url

from bank import views

urlpatterns = patterns('',
                       url(r'^$', views.index, name='index'),
                       url(r'^login/$', views.user_login, name='login'),
                       url(r'^register/$', views.user_register, name='register'),

)
if __name__ == '__main__':
    pass