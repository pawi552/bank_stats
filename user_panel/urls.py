# -*- coding: utf-8 -*-
__author__ = 'bartek'
__date__ = '8/15/14'
__project__ = 'bank_stats'

from django.conf.urls import patterns, url

from user_panel import views


urlpatterns = patterns('',
                       url(r'^$', views.index, name='base'),
                       # url(r'^$', PeopleView.as_view(), name='people', ),
                       # url(r'^id/(?P<id>\d+)/$', PeopleDetails.as_view(), name='peopleDetails', ),
)

if __name__ == '__main__':
    pass